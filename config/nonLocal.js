module.exports={
  user:process.env.PGUSER,
  password:process.env.PGPASSWORD,
  host:process.env.PGHOST,
  database:process.env.PGDATABASE
}