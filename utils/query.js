const Pool=require('pg').Pool
const config=Object.assign(require('../config/default'),require(
  process.env.APP_ENV =='local' ? '../config/local' : '../config/nonLocal'
))
var pool=new Pool(config)
pool.on('error',(err,client)=>console.log('idle client error',err.message,err.stack))

function query(query,vars) {
  return pool.query(query,vars)
}

module.exports=query