const detectBreed = (filename,callback) =>{


  // Load the SDK and UUID
  var AWS = require('aws-sdk');
  AWS.config.update({region:'us-east-1'});

  var fs=require('fs')
  var dogBreeds=require('./breeds.json')
  var catBreeds=require('./catBreeds.json')
  var birdBreeds=require('./birdBreeds.json')
  var reptileBreeds=require('./reptileBreeds.json')
  var horseBreeds=require('./horseBreeds.json')

  const dontUpload=['Nudity','Graphic Male Nudity','Graphic Female Nudity','Sexual Activity','Partial Nudity']

  const animals=['Dog','Cat','Bird','Reptile','Snake','Lizard','Horse','Pony']
  function getBreeds(animal) {
    switch(animal) {
      case 'Dog':
        return dogBreeds;
      case 'Cat':
        return catBreeds;
      case 'Bird':
        return birdBreeds;
      case 'Reptile':
      case 'Snake':
      case 'Lizard':
        return reptileBreeds;
      case 'Horse':
      case 'Pony':
        return horseBreeds;
      default:
        return []
    }
  }
  function getAnimal(labels) {
    const animalObj=labels.find(label=>animals.includes(label.Name))
    if(animalObj==undefined) return undefined;
    else return animalObj.Name;
  }
  function detectBadStuff(labels) {
    const badLabels=labels.find(label=>dontUpload.includes(label.Name))
    return badLabels !== undefined
  }
// Create an S3 client
  var s3 = new AWS.S3();
  var rekognition=new AWS.Rekognition().detectLabels({
    Image:{
      S3Object: {
        Bucket: "breed-photos-east",
        Name: filename
      }
    }
  },(err,data)=>{
    if (err) console.log(err, err.stack); // an error occurred
    else {
      // console.log(data);           // successful response
      if(detectBadStuff(data.Labels)) {
        console.log('bad label deteced, deleting from bucket')
        callback('Sorry, we could not detect the animal in this picture',true)
        return;
      }
      var animal=getAnimal(data.Labels)
      if(animal==undefined) {
        callback('Sorry, we could not detect the animal in this picture')
        return;
      }
      var breeds=getBreeds(animal)
      var likelyBreeds=data.Labels
        .filter(label=>breeds.includes(label.Name))
        .sort((a,b)=>a.Confidence-b.confidence)
        .map(label=>label.Name)
      if(likelyBreeds.length > 0) {
        var mostLikelyBreed=likelyBreeds.shift()
        var otherBreeds=likelyBreeds
        var message=`I'm guessing the breed of your <b>${animal}</b> is <b>${mostLikelyBreed}</b>`
        if(otherBreeds.length > 0) {
          message += ` but it might also be one of the following: <b>${otherBreeds.join(', ')}</b>`
        }
        callback(message)
      } else {
        callback(`Well, I'm pretty sure it's a <b>${animal}</b> but we could not detect the breed, sorry!`)
      }

    }

  });
}

module.exports={
  detectBreed
}