/*
 * Copyright 2013. Amazon Web Services, Inc. All Rights Reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
**/

// Load the SDK and UUID
var AWS = require('aws-sdk');
AWS.config.update({region:'us-east-1'});

var fs=require('fs')
var dogBreeds=fs.readFileSync('breeds.txt','utf8').split('\r\n')
var catBreeds=fs.readFileSync('catBreeds.txt','utf8').split('\r\n')
var birdBreeds=fs.readFileSync('birdBreeds.txt','utf8').split('\r\n')
var reptileBreeds=fs.readFileSync('reptileBreeds.txt','utf8').split('\r\n')
var horseBreeds=require('./horseBreeds.json')

const animals=['Dog','Cat','Bird','Reptile','Snake','Lizard','Horse','Pony']
function getBreeds(animal) {
  switch(animal) {
    case 'Dog':
      return dogBreeds;
    case 'Cat':
      return catBreeds;
    case 'Bird':
      return birdBreeds;
    case 'Reptile':
    case 'Snake':
    case 'Lizard':
      return reptileBreeds;
    case 'Horse':
    case 'Pony':
      return horseBreeds;
    default:
      return []
  }
}
function getAnimal(labels) {
  const animalObj=labels.find(label=>animals.includes(label.Name))
  if(animalObj==undefined) return undefined;
  else return animalObj.Name;
}
// Create an S3 client
var s3 = new AWS.S3();
var rekognition=new AWS.Rekognition().detectLabels({
  Image:{
    S3Object: {
      Bucket: "breed-photos-east",
      Name: "robinSample.jpg"
    }
  }
},(err,data)=>{
  if (err) console.log(err, err.stack); // an error occurred
  else {
    console.log(data);           // successful response
    var animal=getAnimal(data.Labels)
    if(animal==undefined) {
      console.log('Sorry, we could not detect the animal in this picture')
      return;
    }
    var breeds=getBreeds(animal)
    var likelyBreeds=data.Labels
      .filter(label=>breeds.includes(label.Name))
      .sort((a,b)=>a.Confidence-b.confidence)
      .map(label=>label.Name)
    if(likelyBreeds.length > 0) {
      var mostLikelyBreed=likelyBreeds.shift()
      var otherBreeds=likelyBreeds
      var message=`I'm guessing the breed of your ${animal} is ${mostLikelyBreed}`
      if(otherBreeds.length > 0) {
        message += ` but it might also be one of the following: ${otherBreeds.join(', ')}`
      }
      console.log(message)
    } else {
      console.log(`Well, I'm pretty sure it's a ${animal} but we could not detect the breed, sorry!`)
    }

  }

});

// // Create a bucket and upload something into it
// const uuidv4 = require('uuid/v4');
// var bucketName = 'node-sdk-sample-' + uuidv4();
// var keyName = 'hello_world.txt';
//
// s3.createBucket({Bucket: bucketName}, function() {
//   var params = {Bucket: bucketName, Key: keyName, Body: 'Hello World!'};
//   s3.putObject(params, function(err, data) {
//     if (err)
//       console.log(err)
//     else
//       console.log("Successfully uploaded data to " + bucketName + "/" + keyName);
//   });
// });
