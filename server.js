var AWS = require('aws-sdk');
AWS.config.update({region:'us-east-1'});
var s3 = new AWS.S3();
const uuidv4 = require('uuid/v4');
//todo create a separate service to create a phash of each image to compare and dedupe and associate duplicates' corresponding feedback to the deduped record
//todo narrow s3/Rekognition group permission scope
const breedDetector=require('./breedDetector')
const express = require('express')
const fileUpload = require('express-fileupload')
const app = express()
const fs=require('fs')
const resultText=fs.readFileSync('./result.html','utf8')
const query=require('./utils/query')

app.use(fileUpload());
var bodyParser = require('body-parser');
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

app.post('/api/upload', function (req, res) {
  if (!req.files)
    return res.status(400).send('No files were uploaded.');
  // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
  let sampleFile = req.files.sampleFile;
  let newName=sampleFile.name.split('.')[0]+uuidv4()+'.'+sampleFile.name.split('.')[1]
  var params = {Bucket: 'breed-photos-east', Key: newName, Body: sampleFile.data};
  s3.putObject(params, function(err, data) {
    if (err)
      console.log(err)
    else {
      let message="Successfully uploaded data to " + 'breed-photos-east' + "/" + newName;
      console.log(message);
      breedDetector.detectBreed(newName,(breedMessage,isBad)=>{
        if(isBad) {
          s3.deleteObject({Bucket: 'breed-photos-east', Key: newName},(err,data)=>{
            if(err) console.log(err);
            else res.send(resultText.replace('{message}',breedMessage).replace('{encodedName}',''))
          })
        } else {
          //todo move to new bucket that hosts images to get feedback on
          res.send(resultText.replace('{message}',breedMessage).replace('{encodedName}',encodeURIComponent(newName)))
        }
      })
    }
  });
})


app.get('/api/image',(req,res)=>{
  var filename=req.query.filename
  var params = {Bucket: 'breed-photos-east', Key: filename};
  // var file = require('fs').createWriteStream('/uploaded/'+filename);
  s3.getObject(params).createReadStream().pipe(res);
})

app.post('/api/feedback',(req,res)=>{ //todo add request throttling and resubmit prevention
  var params={Bucket: 'breed-photos-east'}
  s3.listObjects(params,(err,data)=>{
    const contents=data.Contents.filter(file=>file.Key !== req.query.filename)
    const newName=contents[Math.floor(Math.random() * contents.length) + 0  ].Key
    function cleanEmUp(guess) {
      return guess.toLowerCase().trim().substr(0,999)
    }
    const guess1=cleanEmUp(req.body.guess1)
    let guess2=cleanEmUp(req.body.guess2)
    let guess3=cleanEmUp(req.body.guess3)
    if(guess1==guess2) {
      guess2=''
    }
    if(guess1==guess3) {
      guess3=''
    }
    if(req.body.flag) {
      let params = {
        Bucket:'breed-photos-east-2',
        CopySource: '/breed-photos-east/'+req.query.filename,
        Key: req.query.filename
      }
      s3.copyObject(params,(err,data)=>{
        if(err) console.log(err);
        else s3.deleteObject({Bucket: 'breed-photos-east', Key: req.query.filename},(err,data)=>{
          if(err) console.log(err);
        })
      })
    } else {
      if(data.Contents.find(file=>file.Key==req.query.filename) && (guess1 !== '' || guess2 !== '' || guess3 !== '')) {
        query(`INSERT INTO breed.feedback (id,filename,guess1,guess2,guess3) VALUES (nextval('breed.feedback_id_seq'),$1,$2,$3,$4)`,
          [req.query.filename.substr(0,999),cleanEmUp(req.body.guess1),req.body.guess2.substr(0,999),req.body.guess3.substr(0,999)])
      } else {
        console.log('invalid filename - does not exist:'+req.query.filename)
      }
    }

    res.send(fs.readFileSync('./thanks.html','utf8').replace('{encodedName}',encodeURIComponent(newName)))
  })
})
app.use(express.static('public'))
app.listen(process.env.PORT || 3000, function () {
  console.log('Example app listening on port 3000!')
})